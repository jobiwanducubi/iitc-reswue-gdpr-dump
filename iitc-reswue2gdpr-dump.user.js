// ==UserScript==
// @id             iitc-plugin-reswue2gdprdump
// @name           IITC plugin: ResWue Alerts to GDPR dump format
// @author         JobiwanDucubi
// @category       Converter
// @version        0.1.0.20180919.007
// @namespace      https://bitbucket.org/jobiwanducubi/iitc-reswue-gdpr-dump/
// @downloadURL    https://bitbucket.org/jobiwanducubi/iitc-reswue-gdpr-dump/raw/master/iitc-reswue2gdpr-dump.user.js
// @description    [0.1.0.20180919.007] ResWue Alerts to GDPR dump format
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// ==/UserScript==

function wrapper(plugin_info) {
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    plugin_info.buildName = 'JobiwanDucubi';
    plugin_info.dateTimeVersion = '20180919.007';
    plugin_info.pluginId = 'reswue2gdprdump';

    // PLUGIN START ////////////////////////////////////////////////////////
    // use own namespace for plugin
    window.plugin.reswue2gdpr = function() {};

    // FUNCTIONS ////////////////////////////////////////////////////////
    // init setup
    window.plugin.reswue2gdpr.setup = function(doNotRetry) {
        console.log('ResWue Alerts to GDPR dump');

        if (window.plugin.reswue) {
            window.plugin.reswue2gdpr.addButton();
        } else {
            if (doNotRetry) {
                alert('Reswue Alerts to GDPR dump requires Reswue plugin');
            } else {
                console.log('ResWue not ready! Next attempt in one second...');
                setTimeout(window.plugin.reswue2gdpr.setup, 1000, true);
            }
        }
    };

    // copy settings
    window.plugin.reswue2gdpr.exportAlerts = function() {
        if (window.plugin.reswue.core._selectedOperation == undefined) {
            alert('Please select an operation before export!');
            return;
        }
        
        var alerts = window.plugin.reswue.core._selectedOperation.api.operationContext.alerts;

        if (alerts.length == 0) {
            alert('There is no alert to proceed. Please check operation data!');
            return;
        }

        var dump = "";

        console.log("[ResWue GDPR] " + alerts.length + " alert(s) to proceed");

        for (var i in alerts) {
            var _alert = alerts[i];

            if (_alert.resolvedDate != undefined) {
                var dateTime = new Date(_alert.resolvedDate*1000);
                var sDateTime = window.plugin.reswue2gdpr.formatDate(dateTime)
                var lng = _alert.portal.lng;
                var lat = _alert.portal.lat;
                // FarmPortalAlert -> Visit (hack neutral)
                // UpgradePortalAlert -> Capture

                var op;

                if (_alert.type == "FarmPortalAlert") {
                    op = "hacked neutral portal\tsuccess";
                } else if (_alert.type == "UpgradePortalAlert") {
                    op = "captured portal\tNone";
                } else {
                    continue;
                }

                dump += sDateTime + "\t" + lat + "\t" + lng + "\t" + op + "\n";
            }
        }

        var html = '<p><a onclick="$(\'#reswue-gdpr-dump-copy\').select();">Select all</a> and press CTRL+C to copy it.</p>';
        html += '<textarea style="width:700px; height:150px;" id="reswue-gdpr-dump-copy" readonly onclick="$(\'#reswue-gdpr-dump-copy\').select();">' + dump + '</textarea>';

        dialog({
            html: html,
            width: 720,
            dialogClass: 'dialog-reswue-gdpr-dump',
            title: 'ResWue Alerts Export'
        });
    };

    // toolbox menu
    window.plugin.reswue2gdpr.addButton = function() {
        $('div.leaflet-bar.leaflet-control').each(function() {
            var thisdiv = $(this);
            $(this).children().each(function() {
                if ($(this).attr('title') == 'RESWUE') {
                    thisdiv.append('<a onclick="window.plugin.reswue2gdpr.exportAlerts();" class="leaflet-bar-part" title="Export ResWue Alerts to GDPR dump format" href="#"><img id="reswue-btn-gdpr-dump" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDY3LjY3MSA2Ny42NzEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDY3LjY3MSA2Ny42NzE7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4Ij4KPGc+Cgk8cGF0aCBkPSJNNTIuOTQ2LDIzLjM0OEg0Mi44MzR2NmgxMC4xMTJjMy4wMDcsMCw1LjM0LDEuNTM2LDUuMzQsMi44NTh2MjYuNjA2YzAsMS4zMjItMi4zMzMsMi44NTgtNS4zNCwyLjg1OEgxNC43MjQgICBjLTMuMDA3LDAtNS4zNC0xLjUzNi01LjM0LTIuODU4VjMyLjIwN2MwLTEuMzIyLDIuMzMzLTIuODU4LDUuMzQtMi44NThoMTAuMTF2LTZoLTEwLjExYy02LjM1OSwwLTExLjM0LDMuODkxLTExLjM0LDguODU4djI2LjYwNiAgIGMwLDQuOTY4LDQuOTgxLDguODU4LDExLjM0LDguODU4aDM4LjIyM2M2LjM1OCwwLDExLjM0LTMuODkxLDExLjM0LTguODU4VjMyLjIwN0M2NC4yODYsMjcuMjM5LDU5LjMwNSwyMy4zNDgsNTIuOTQ2LDIzLjM0OHoiIGZpbGw9IiM1ZTY0NjkiLz4KCTxwYXRoIGQ9Ik0yNC45NTcsMTQuOTU1YzAuNzY4LDAsMS41MzUtMC4yOTMsMi4xMjEtMC44NzlsMy43NTYtMy43NTZ2MTMuMDI4djZ2MTEuNDk0YzAsMS42NTcsMS4zNDMsMywzLDNzMy0xLjM0MywzLTNWMjkuMzQ4di02ICAgVjEwLjExN2wzLjk1OSwzLjk1OWMwLjU4NiwwLjU4NiwxLjM1NCwwLjg3OSwyLjEyMSwwLjg3OXMxLjUzNS0wLjI5MywyLjEyMS0wLjg3OWMxLjE3Mi0xLjE3MSwxLjE3Mi0zLjA3MSwwLTQuMjQybC04Ljk1Ny04Ljk1NyAgIEMzNS40OTIsMC4yOTEsMzQuNzI1LDAsMzMuOTU4LDBjLTAuMDA4LDAtMC4wMTUsMC0wLjAyMywwcy0wLjAxNSwwLTAuMDIzLDBjLTAuNzY3LDAtMS41MzQsMC4yOTEtMi4xMiwwLjg3N2wtOC45NTcsOC45NTcgICBjLTEuMTcyLDEuMTcxLTEuMTcyLDMuMDcxLDAsNC4yNDJDMjMuNDIyLDE0LjY2MiwyNC4xODksMTQuOTU1LDI0Ljk1NywxNC45NTV6IiBmaWxsPSIjNWU2NDY5Ii8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" width="16" height="16" style="vertical-align:middle;align:center;"></a>');
                }
            });
        });
    };

    window.plugin.reswue2gdpr.formatDate = function(date) {
        var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();

        var sDate = year;
        sDate += " ";
        sDate += month < 10 ? "0" + month : month;
        sDate += "-";
        sDate += day < 10 ? "0" + day : day;
        sDate += " ";
        sDate += hours < 10 ? "0" + hours : hours;
        sDate += ":";
        sDate += minutes < 10 ? "0" + minutes : minutes;
        sDate += ":";
        sDate += seconds < 10 ? "0" + seconds : seconds;

        return sDate;
    };

    // runrun
    var setup = window.plugin.reswue2gdpr.setup;

    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC and ResWue already booted, immediately run the 'setup' function
    if (window.iitcLoaded != undefined && typeof setup === 'function') {
        setup();
    }

    // PLUGIN END ////////////////////////////////////////////////////////
} // WRAPPER END ////////////////////////////////////////////////////////

var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
